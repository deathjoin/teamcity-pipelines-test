import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildFeatures.pullRequests
import jetbrains.buildServer.configs.kotlin.buildSteps.script
import jetbrains.buildServer.configs.kotlin.triggers.vcs

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2022.04"

project {

    buildType(Mr)

    subProject(Develop)
}

object Mr : BuildType({
    name = "MR"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            name = "print"
            scriptContent = """echo "la-la-la""""
        }
        script {
            name = "another build 23"
            scriptContent = """echo "q1324123""""
        }
    }

    triggers {
        vcs {
            branchFilter = "+:merge-requests/*"
        }
    }

    features {
        pullRequests {
            vcsRootExtId = "${DslContext.settingsRoot.id}"
            provider = gitlab {
            }
            param("authenticationType", "vcsRoot")
        }
    }
})


object Develop : Project({
    name = "Develop"

    buildType(Develop_BuildServer)
})

object Develop_BuildServer : BuildType({
    name = "Build server"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            name = "build"
            scriptContent = """
                echo "12222"
                echo "33"
            """.trimIndent()
        }
        script {
            name = "test"
            scriptContent = """
                echo "test"
                echo "done"
            """.trimIndent()
        }
    }
})
